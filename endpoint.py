class Endpoint:
    def __init__(self):
        self.destination = 'output.log'

    def send_arrival(self, announcement):
        with open(self.destination, 'a') as f:
            f.write(announcement)

    def send_following_arrivals(self, multiple_announcements):
        with open(self.destination, 'a') as f:
            for line in multiple_announcements:
                f.write(line)


endpoint = Endpoint()
