#!/usr/bin/env python
# encoding: utf-8

import time
import threading
import requests
import sched
import sys
from endpoint import endpoint


def fetch_predictions(stop_point_id, line_id):
    """
    Fetches the next 3 predicted arrivals at a particular stop point for a
    particular line.
    """
    endpoint = (
        "https://api.tfl.gov.uk/Line/{line_id}/"
        "Arrivals/{stop_point_id}?direction=inbound"
    ).format(
        stop_point_id=stop_point_id,
        line_id=line_id
    )
    relevant = sorted(
        requests.get(endpoint).json(),
        key=lambda d: d.get('timeToStation', 0)
    )[:3]
    return relevant


def make_announcement(p, endpoint=endpoint):
    msg = 'Train for {destination} is arriving at {platform_name} at {time}\n'.format(
        destination=p['destinationName'],
        platform_name=p['platformName'],
        time=p['timestamp'],
    )
    endpoint.send_arrival(msg)


def add_and_monitor_prediction(scheduler, prediction, manager):
    notify_at = (prediction['timeToStation'] - 5)
    event = scheduler.enter(
        notify_at,
        1,
        make_announcement,
        argument=(prediction,)
    )
    manager[prediction['id']] = {
        'time': prediction['timeToStation'],
        'event': event,
    }


def schedule_predictions(scheduler, manager, stop_point_id, line_id):
    """
    Fetches the predictions, and maintains the schedule manager.

    The scheduler is the equivalent of a cron job in python.

    The manager is a mechanism for keeping track of the current predictions and
    updating them if the predictions change.
    """
    predictions = fetch_predictions(stop_point_id, line_id)
    for p in predictions:
        prediction = manager.get(p['id'])

        if not prediction:
            add_and_monitor_prediction(scheduler, p, manager)

        elif prediction and prediction['time'] != p['timeToStation']:
            scheduler.cancel(prediction['event'])
            add_and_monitor_prediction(scheduler, p, manager)

        else:
            pass


def main(stop_point_id, line_id):
    schedule_manager = {}
    s = sched.scheduler(time.time, time.sleep)

    def loop(period=60):
        """
        This loop is responsible for:
        * periodically fetching from the API endpoint to determine if
          predictions have been added/changed, and
        * Synchronising the scheduler with the schedule_manager.

        This will actually "drift" over time, but that isn't an issue for this
        application.
        """
        print('Scheduling predictions')
        schedule_predictions(s, schedule_manager, stop_point_id, line_id)
        print('Scheduling predictions completed')
        s.run()
        threading.Timer(period, loop).start()
    loop()


if __name__ == '__main__':
    main(*sys.argv[1:])
